//
//  ViewController.swift
//  tantan
//
//  Created by wenyuzhao on 02/05/2018.
//  Copyright © 2018 wenyuzhao. All rights reserved.
//

import UIKit
import WebKit

class ViewController: UIViewController {
   @IBOutlet weak var webView: WKWebView!
   
   override func viewDidLoad() {
      super.viewDidLoad()
      // Do any additional setup after loading the view, typically from a nib.
      let htmlString = "欢迎使用探探, 在使用过程中有疑问请<a href=\"tantan://feedback\">反馈</a>"
      webView.loadHTMLString(htmlString, baseURL:nil)
      webView.backgroundColor = .gray
   }

   override func didReceiveMemoryWarning() {
      super.didReceiveMemoryWarning()
      // Dispose of any resources that can be recreated.
   }


}

